# Introduction

A DDS (double-dummy solver) function is available through [Bo Haglund's DDS 2.9.0](http://privat.bahnhof.se/wb758135/bridge/dll.html). For Windows, the DDS DLL is distributed with  are distributed together.

The solution used here is copied from below two opensource project, credits to them

* https://github.com/Afwas/python-dds 
* https://github.com/anntzer/redeal

this project focus on DDS table only like below

````
$ ./ddstable.py
          S     H     D     C    NT
    N     -     4     2     -     3
    S     -     4     2     -     3
    E     3     -     -     -     -
    W     3     -     -     -     -
````

How to use it

````
from ddstable import ddstable
PBN = b"E:QJT5432.T.6.QJ82 .J97543.K7532.94 87.A62.QJT4.AT75 AK96.KQ8.A98.K63"
all = ddstable.get_ddstable(PBN)
print("{:>5} {:>5} {:>5} {:>5} {:>5} {:>5}".format("", "S", "H", "D", "C", "NT"))
# may use  card_suit=["C", "D", "H", "S", "NT"]
for each in all.keys():
    print("{:>5}".format(each),end='')
    for suit in ddstable.dcardSuit:
        trick=all[each][suit]
        if trick>7:
            print(" {:5}".format(trick - 6),end='')
        else:
            print(" {:>5}".format("-"),end='')
    print("")
````

or 

````
>>> from ddstable import ddstable
>>> PBN = b"E:QJT5432.T.6.QJ82 .J97543.K7532.94 87.A62.QJT4.AT75 AK96.KQ8.A98.K63"
>>> ddstable.get_ddstable(PBN)
{'N': {'S': 4, 'H': 10, 'D': 8, 'C': 6, 'NT': 9}, 'S': {'S': 4, 'H': 10, 'D': 8, 'C': 6, 'NT': 9}, 'E': {'S': 9, 'H': 2, 'D': 3, 'C': 7, 'NT': 3}, 'W': {'S': 9, 'H': 2, 'D': 3, 'C': 7, 'NT': 3}}
````

# FAQ

In order to make it simple, I embed compiled `libdds.so` and `dds-64.dll`, it works in most of platform.

Normally library shall be compiled during installation, check [redeal](https://github.com/anntzer/redeal)'s solution.

## In Unix, it doesn't work

You may need to install/upgrade to high version of `libstdc++` or compile by yourself, I embed default `libdds.so`

````
# libdds.so needs GLIBCXX_3.4.21+
$ strings /lib/x86_64-linux-gnu/libstdc++.so.6  | grep GLIBCXX
..
GLIBCXX_3.4.28
````

in some system, if library is installed in extra place, then set `LD_LIBRARY_PATH`

````
export LD_LIBRARY_PATH=/app/RHEL7-x86_64/gcc/10.2.0/lib64
````

## MacOS

Fix when I have mac

## Windows

works for 64bit, so it shall cover most of you.

# Reference

* https://github.com/Afwas/python-dds 
* https://github.com/anntzer/redeal