# Introduction

# Development

prepare the token in advance from https://pypi.org/manage/account/

````
$ cat ~/.pypirc
[pypi]
  username = __token__
  password = pypi-xxxx
````

## Unix

````
python3 setup.py sdist bdist_wheel
python3 -m twine upload --skip-existing --verbose dist/*
````

## Windows

````
pip install wheel twine
python setup.py sdist bdist_wheel
python -m twine upload --skip-existing --verbose dist/*
````

# FAQ

Under unix env, may setup

````
export PYTHONIOENCODING=utf8
````
